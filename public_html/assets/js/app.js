var lasteditid;
function get_request(url,action,id){ 
    $.ajax({
        url: url,
        type: 'Get',
        success: function (data, textStatus, request) {
            if(data["status"]=="OK"){
                $('#ok').removeClass('hide-alert');
                $('#ok-text').html(data["msg"]);
                $('#btn-'+data["table-name"]).attr("disabled",true);
                $('#btn-'+data["table-name"]).html(data["msg"]);
                if(action=='delete'){
                    $('#'+id).remove();
                }
                if(action=='del-img'){
                    $('#'+id).remove();
                }
                if(action=='show'){
                    $('#table-posts').empty();
                    data["posts"].forEach(element =>update_posts(element));
                }
                if(action == 'show-child-posts'){
                    $('#table-child-posts').empty();
                    data["posts"].forEach(element =>update_posts(element));
                }
                if(action=='show-page'){
                    $('#page-id').val(data['page']["id"]);
                    $('#page-title').val(data['page']["title"]);
                    $('#page-slug').val(data['page']["slug"]);
                    $('#page-cat').val(data['page']["cat_id"]);
                    $('#page-description').val(data['page']["description"]);
                    var editor = tinymce.get('postcontent');
                    var content = data['page']["content"];
                    editor.setContent(content);
                    $('#page-preview').attr('src', "/uploads/"+data['page']["image"]);
                }
                if(action=='edit-cat'){
                    $('#edit-cat-id').val(data['cat']["id"]);
                    $('#edit-cat-name').val(data['cat']["name"]);
                    $('#edit-cat-description').val(data['cat']["description"]);
                    $('#edit-cat-preview').attr('src', "/uploads/"+data['cat']["image"]);
                }
                if(action=='edit-post'){
                    $('#edit-post-id').val(data['post']["id"]);
                    $('#edit-post-title').val(data['post']["title"]);
                    $('#edit-post-cat').val(data['post']["cat_id"]);
                    $('#edit-post-description').val(data['post']["description"]);
                    var editor = tinymce.get('edit-post-content');
                    var content = data['post']["content"];
                    editor.setContent(content);
                    $('#edit-post-preview').attr('src', "/uploads/"+data['post']["image"]);
                }
            }else{
                $('#fail').removeClass('hide-alert');
                $('#fail-text').html(data["msg"]);
            }
        },
        error: function (response) {
            $('#fail').removeClass('hide-alert');
            $('#fail-text').html(data["msg"]);
        },
    });
}
function post_request(url,modal,form){
    tinyMCE.triggerSave();
    var data =  $('#'+form).serializeArray();
    $.ajax({
        url: url,
        type: 'post',
        data: data,
        success: function (data, textStatus, request) {
            if(data["status"]=="OK"){
                $('#ok').removeClass('hide-alert');
                $('#ok-text').html(data["msg"]);
                if(modal == 'edit-cat-modal'){
                    uploadImage('cat', data["cat"]["slug"] ,data["cat_id"]);
                    $('#cat-name-'+data["cat_id"]).text(data["name"]);
                }
                if(modal == 'edit-post-modal'){
                    uploadImage('post',data["post"]["slug"],data["post_id"]);
                    $('#post-title-'+data["post_id"]).html(data["title"]);
                    $('#post-link-'+data["post_id"]).html(data["title"]);
                }
                if(modal == 'cat-modal'){
                    uploadImage('cat', data["slug"], data["cat_id"]);
                    $("#menu").append('\
                    <div class="btn-group" role="group" id="cat-'+data["cat_id"]+'">\
                        <button id="cat-name-'+data["cat_id"]+'" type="button" class="btn btn-outline-primary" onclick="get_request("/home/cat/'+data["slug"]+'")","show">'+data["name"]+'</button>\
                        <button type="button" data-toggle="modal" data-target="#edit-cat-modal" style="width: 50px;" class="btn btn-outline-warning" onclick="get_request(\'/home/edit/cat-'+data["cat_id"]+'\',\'edit-cat\',\'#cat-'+data["cat_id"]+'\')">E</button>\
                        <button type="button" style="width: 50px;" class="btn btn-outline-danger" onclick="get_request(\'/home/delete/cat-'+data["cat_id"]+'\',\'delete\',\'cat-'+data["cat_id"]+'\')">X</button>\
                    </div>'
                    );
                    $("#cat").append('<option value="'+data["cat_id"]+'">'+data["name"]+'</option>');
                    
                }
                if(modal == 'post-modal'){
                    uploadImage('post',data["slug"],data["post_id"]);
                    if(data["parent_id"]==0){
                        table = "#table-posts";
                        item = "post-"+data["post_id"];
                        request = 'onclick="get_request(\'/home/posts/post-'+data["post_id"]+'\',\'show-child-posts\')"';
                    }else{
                        table = "#table-child-posts";
                        item = "child-post-"+data["post_id"];
                        request = '';
                    }
                    $(table).append(
                        '<tr id="'+item+'" '+request+' >\
                            <td><a id="post-link-'+data["post_id"]+'">'+data["title"]+'</a></td>\
                            <td>\
                                <div class="btn-group btn-group-sm" role="group">\
                                    <button type="button" data-toggle="modal" data-target="#edit-post-modal" class="btn btn-warning" onclick="get_request(\'/home/edit/post-'+data["post_id"]+'\',\'edit-post\',\'#post-'+data["post_id"]+'\')">Ред.</button>\
                                    <button type="button" class="btn btn-danger" onclick="get_request(\'/home/delete/post-'+data["post_id"]+'\',\'delete\',\''+item+'\')">Удалить</button>\
                                </div>\
                            </td>\
                        </tr>'
                    );
                    if(data["parent_id"]==0){
                        $("#post-opt").append('<option value="'+data["post_id"]+'">'+data["title"]+'</option>');
                    }
                }
                if(modal == 'add-page'){
                    $("#menu").append('\
                    <div class="btn-group" role="group" id="page-'+data["page"]["id"]+'">\
                        <button id="page-name-'+data["page"]["id"]+'" type="button" class="btn btn-outline-primary" onclick="get_request(\'/home/page/show/'+data["page"]["id"]+'\',\'show-page\','+data["page"]["id"]+')">'+data["page"]["title"]+'</button>\
                        <button type="button" style="width: 50px;" class="btn btn-outline-danger" onclick="get_request(\'/home/page/delete/'+data["page"]["id"]+'\',\'delete\',\'page-'+data["page"]["id"]+'\')">X</button>\
                    </div>\
                    ');
                }
                if(modal == 'update-page'){
                    $('#page-name-'+data["page_id"]).text(data["title"]);
                }
            }else{
                $('#fail').removeClass('hide-alert');
                $('#fail-text').html(data["msg"]);
            }
            $('#'+modal).modal('hide')
        },
        error: function (response) {
            $('#fail').removeClass('hide-alert');
            $('#fail-text').html(data["msg"]);
        },
    });
}
//Upload image
function uploadImage(element,slug,id){
    if(element=='page'){
        var imgs = $('#upload-img');
        files = imgs[0].files;
    }
    var datafiles = new FormData();
    var url = '/home/upload-image/'+element+'/'+id+'/'+slug;
    $.each( files, function( key, value ){
        datafiles.append( key, value );
    });
    $.ajax({
        url: url,
        method: 'POST',
        data: datafiles,
        dataType: 'JSON',
        contentType: false,
        cache: false,
        processData: false,
        success: function(data){
            id = $('.media-image').length+1;
            $('#media-preview').append('\
            <div class="media-image" id="img-'+id+'">\
                <img src="/uploads/pages/'+data["image"]+'" alt="">\
                <button type="button" class="close" onclick="get_request(\'/home/del-img/pages/'+data["image"]+'\',\'del-img\',\'img-'+id+'\')">\
                    <span aria-hidden="true" id="del-img">&times;</span>\
                </button>\
            </div>\
            ');
        }
    })
}

function update_posts(data){
    if(data["parent_id"]==0){
        table = '#table-posts';
        item = "post-"+data["id"];
        request = 'onclick="get_request(\'/home/posts/post-'+data["id"]+'\',\'show-child-posts\')"';
    }else{
        table = '#table-child-posts';
        item = "child-post-"+data["id"];
        request = '';
    }
    $(table).append(
        '<tr id="'+item+'" '+request+' >\
            <td><a id="post-link-'+data["id"]+'" >'+data["title"]+'</a></td>\
            <td>\
                <div class="btn-group btn-group-sm" role="group">\
                    <button type="button" data-toggle="modal" data-target="#edit-post-modal" class="btn btn-warning" onclick="get_request(\'/home/edit/post-'+data["id"]+'\',\'edit-post\',\'#post-'+data["id"]+'\')">Ред.</button>\
                    <button type="button" class="btn btn-danger" onclick="get_request(\'/home/delete/post-'+data["id"]+'\',\'delete\',\''+item+'\')">Удалить</button>\
                </div>\
            </td>\
        </tr>');
}
$('#close-fail').on('click', function(e){
    $('#fail').addClass('hide-alert');
});
$('#close-ok').on('click', function(e){
    $('#ok').addClass('hide-alert');
});
tinymce.init({
    selector: '#postcontent',
    height: 600,
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table paste imagetools wordcount"
      ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
      
});
tinymce.init({
    selector: '#edit-post-content',
    height: 600,
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table paste imagetools wordcount"
      ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
});
//For tinymce plugin link in modal
$(document).on('focusin', function(e) {
    if ($(e.target).closest(".tox-dialog").length) {
        e.stopImmediatePropagation();
    }
});
function sluggable(input_id,out_id){
    text = $(input_id).val();
    slug = getSlug(text);
    $(out_id).val(slug);
}
//Preview image
var files;
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#cat-preview').attr('src', e.target.result);
            $('#edit-cat-preview').attr('src', e.target.result);
            $('#post-preview').attr('src', e.target.result);
            $('#edit-post-preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#cat-img").change(function(){
    files = this.files;
    readURL(this);
});
$("#edit-cat-img").change(function(){
    files = this.files;
    readURL(this);
});
$("#edit-post-img").change(function(){
    files = this.files;
    readURL(this);
});
$("#post-img").change(function(){
    files = this.files;
    readURL(this);
});