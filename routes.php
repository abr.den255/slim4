<?php
namespace App;

use Slim\Routing\RouteCollectorProxy;
use App\AuthMiddleware;

$app->get('/', \IndexController::class . ':main');

$app->get('/setup', \SetupController::class . ':main');
$app->post('/setup', \SetupController::class . ':setup');

$app->get('/add-user', \SetupController::class . ':show');
$app->post('/add-user', \SetupController::class . ':add_user');

$app->get('/login', \AuthController::class . ':show');
$app->post('/login', \AuthController::class . ':login');

$app->group('/', function (RouteCollectorProxy $group) {
    //Home navbar
    $group->get('home', \HomeController::class . ':home');
    $group->get('home/setup-off', \SetupController::class . ':disable_setup');
    $group->get('home/logout', \AuthController::class . ':logout');
    $group->get('home/{action}-user/{id}', \SetupController::class . ':crud_users');
    $group->get('home/db-status', \SetupController::class . ':show_db');
    $group->get('home/{action}-table/{name}', \SetupController::class . ':create_table');
    $group->get('home/page/{action}/{id}', \HomeController::class . ':pages');
    $group->post('home/page/{action}', \HomeController::class . ':pages');
    //Categories
    $group->get('home/cat/{cat-slug}', \HomeController::class . ':show_posts');
    $group->post('home/add-cat', \HomeController::class . ':add_cat');
    $group->get('home/edit/cat-{id}', \HomeController::class . ':edit_cat');
    $group->post('home/update/cat', \HomeController::class . ':update_cat');
    $group->get('home/delete/cat-{id}', \HomeController::class . ':del_cat');
    //Posts
    $group->get('home/posts/{item}-{id}', \HomeController::class . ':show_posts');
    $group->post('home/add-post', \HomeController::class . ':add_post');
    $group->get('home/edit/post-{id}', \HomeController::class . ':edit_post');
    $group->post('home/update/post', \HomeController::class . ':update_post');
    $group->get('home/delete/post-{id}', \HomeController::class . ':del_post');
    //Upload
    $group->post('home/upload-image/{item}/{id}/{slug}', \HomeController::class . ':upload');
    $group->get('home/del-img/{item}/{image}', \HomeController::class . ':del_img');
})->add(new AuthMiddleware($container));

$app->get('/blog', \IndexController::class . ':show_post');
$app->get('/blog/{cat-slug}', \IndexController::class . ':show_post');
$app->get('/blog/{cat-slug}/{post-slug}', \IndexController::class . ':show_post');
$app->get('/{page}', \IndexController::class . ':show_page');
?>