#!/bin/sh

# GitFlow Wrapper
# Version: 1.0.6
# Author:  Anton Taranovskyi <at@medicosearch.ch>

flow="$1"
flowAction="$2"
case $flow in
    release) ;;
    hotfix) ;;
    *) echo "flow [$flow] is not allowed"; exit 1;;
esac
case $flowAction in
    start) ;;
    finish) ;;
    *) echo "flow action [$flowAction] is not allowed"; exit 1;;
esac

gitflowAvailable=$(git flow version | grep -c "flow")

if ! [ $gitflowAvailable = "0" ] ; then
    echo "git flow is not installed"
    echo "please install"
    echo "wget --no-check-certificate -q  https://raw.githubusercontent.com/petervanderdoes/gitflow-avh/develop/contrib/gitflow-installer.sh && sudo bash gitflow-installer.sh install stable; rm gitflow-installer.sh"
    exit 1
fi

gitflowVersion=1.9.0
gitflowCurrentVersion=$(git flow version | grep -Eo '[0-9\.]+')

echo "git flow version: $gitflowCurrentVersion"

sortedCriteria=$(echo "$gitflowCurrentVersion\n$gitflowVersion" | sort -V | head -n1)

if ! [ $gitflowVersion=$sortedCriteria ] ; then
    echo "git flow version is too old"
    echo "please update"
    echo "wget --no-check-certificate -q  https://raw.githubusercontent.com/petervanderdoes/gitflow-avh/develop/contrib/gitflow-installer.sh && sudo bash gitflow-installer.sh install stable; rm gitflow-installer.sh"
    exit 1
fi

git config user.email "ciadentro@qix.sx"
git config user.name "CI Adentro"

git flow init -d

currentGen=$(git tag | grep -Eo '^[0-9]+' | sort -rn | head -n 1)
currentRelease=$(git tag | grep -Eo '\.[0-9]+\.' | grep -Eo '[0-9]+' | sort -rn | head -n 1)
currentHotfix=$(git tag | grep "$currentGen.$currentRelease." | grep -Eo '[0-9]+$' | sort -rn | head -n 1)

echo "current tag: $currentGen.$currentRelease.$currentHotfix"

currentBranch="$CI_COMMIT_REF_NAME"
currentCommitHash="$CI_COMMIT_SHA"
if [ -z "${currentBranch}" ]; then
    currentBranch=$(git rev-parse --abbrev-ref HEAD)
    currentCommitHash=$(git rev-parse HEAD)
fi
echo "current branch: $currentBranch"
echo "current commit: $currentCommitHash"

git checkout develop 2>/dev/null || git checkout -t origin/develop
git checkout master 2>/dev/null || git checkout -t origin/master
git checkout $currentCommitHash

if [ $flow = "release" ] ; then
    if [ $flowAction = "start" ] ; then
        if [ $currentBranch = "develop" ] ; then
            lastCommitHash=$(git rev-parse origin/develop)
            echo "last commit: $lastCommitHash"
            if [ $lastCommitHash=$currentCommitHash ] ; then
                nextRelease="$currentGen.$(($currentRelease+1)).0"
                git flow release start $nextRelease
                git flow release publish $nextRelease
                exit 0
            else
                echo "develop commit is not the last"
                exit 1
            fi
        else
            echo "release could be started from the develop only"
            exit 1
        fi
    fi
    if [ $flowAction = "finish" ] ; then
        prefix=$(echo "$currentBranch" | cut -d"/" -f 1)
        if [ $prefix = "release" ] ; then
            tag=$(echo "$currentBranch" | cut -d"/" -f 2)
            lastCommitHash=$(git rev-parse origin/release/$tag)
            echo "last commit: $lastCommitHash"
            if [ $lastCommitHash=$currentCommitHash ] ; then
                echo "finishing the release $tag"
                git checkout release/$tag 2>/dev/null || git checkout -t origin/release/$tag
                git pull
                git flow release finish $tag -p -m "$tag"
                git push origin --tags
                exit 0
            else
                echo "release $tag commit is not the last"
                exit 1
            fi
        else
            echo "this is not a release branch"
            exit 1
        fi
    fi
fi

if [ $flow = "hotfix" ] ; then
    if [ $flowAction = "start" ] ; then
        if [ $currentBranch = "master" ] ; then
            lastCommitHash=$(git rev-parse origin/master)
            echo "last commit: $lastCommitHash"
            if [ $lastCommitHash=$currentCommitHash ] ; then
                nextRelease="$currentGen.$currentRelease.$(($currentHotfix+1))"
                git flow hotfix start $nextRelease
                git flow hotfix publish $nextRelease
                exit 0
            else
                echo "master commit is not the last"
                exit 1
            fi
        else
            echo "release could be started from the master only"
            exit 1
        fi
    fi
    if [ $flowAction = "finish" ] ; then
        prefix=$(echo "$currentBranch" | cut -d"/" -f 1)
        if [ $prefix = "hotfix" ] ; then
            tag=$(echo "$currentBranch" | cut -d"/" -f 2)
            lastCommitHash=$(git rev-parse origin/hotfix/$tag)
            echo "last commit: $lastCommitHash"
            if [ $lastCommitHash=$currentCommitHash ] ; then
                echo "finishing the hotfix $tag"
                git checkout hotfix/$tag 2>/dev/null || git checkout -t origin/hotfix/$tag
                git pull
                git flow hotfix finish $tag -p -m "$tag"
                git push origin --tags
                exit 0
            else
                echo "hotfix $tag commit is not the last"
                exit 1
            fi
        else
            echo "this is not a hotfix branch"
            exit 1
        fi
    fi
fi
