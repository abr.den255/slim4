<?php
namespace App\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Auth;

use App\Models\Category;
use App\Models\User;
use App\Models\Post;
use App\Models\Page;

class HomeController extends Controller
{
    public function home(Request $request, Response $response,$args){
        return $this->view->render($response, 'admin/home.twig', [
            'cats' => $cats,
            'posts' => $posts,
        ]);
    }
    public function pages(Request $request, Response $response,$args){
        if($args["action"]=='show'){
            if($args["id"]=='all')
                $pages = Page::all();
            elseif($args["id"]!=null){
                $page = Page::find($args["id"]);
                $payload = json_encode(['status' => "OK",'msg'=>"All ok!",'page'=>$page], JSON_PRETTY_PRINT);
                $response->getBody()->write($payload);
                return $response->withHeader('Content-Type', 'application/json');
            }
        }elseif($args["action"]=='add'){
            $body = $request->getParsedBody();
            $status = "OK";
            $post = Page::where('slug',$body["slug"])->first();
            if($post == null){
                Page::create([
                    'title' => $body["title"],
                    'slug' => $body["slug"],
                    'description' => $body["description"],
                    'content' => $body["content"],
                ]);
                $msg = "Post added!";
                $page = Page::select('id','title')->where('slug',$body["slug"])->first();
            }else{
                $msg = "Post with slug ".$body["slug"]." exists!";
                $status = "fail";
            }
            $payload = json_encode(['status' => $status,'msg'=>$msg,'page'=>$page], JSON_PRETTY_PRINT);
            $response->getBody()->write($payload);
            return $response->withHeader('Content-Type', 'application/json');
        }elseif($args["action"]=='delete'){
            Page::find($args["id"])->delete();
            $msg = "Post deleted!";
            $payload = json_encode(['status' => 'OK','msg'=>$msg,], JSON_PRETTY_PRINT);
            $response->getBody()->write($payload);
            return $response->withHeader('Content-Type', 'application/json');
        }elseif($args["action"]=='update'){
            $body = $request->getParsedBody();
            Page::find($body["id"])->update([
                'title' => $body["title"],
                'slug' => $body["slug"],
                'description' => $body["description"],
                'content' => $body["content"],
            ]);
            $page =Page::select('slug')->where('id',$body["id"])->first();
            $payload = json_encode(['status' => 'OK','msg'=>'Post updated!','page_id'=>$body["id"],'title'=>$body["title"],'post'=>$page], JSON_PRETTY_PRINT);
            $response->getBody()->write($payload);
            return $response->withHeader('Content-Type', 'application/json');
        }
        $page_images_path = $this->get_upload_folder().'/pages';
        $images = scandir($page_images_path);
        return $this->view->render($response, 'admin/pages.twig', [
            'pages' => $pages,
            'images' => $images,
        ]);
    }
    public function show_posts(Request $request, Response $response, $args){
        if($args["id"] == "all"){
            if(Category::exist())
                $cats = Category::all();
            if(Post::exist()){
                $posts = Post::all();
                foreach($posts as $post){
                    $post_cat = Category::select('slug')->where('id',$post["cat_id"])->first();
                    $post["cat_id"] = $post_cat["slug"];
                }
            }
            return $this->view->render($response, 'admin/posts.twig', [
                'cats' => $cats,
                'posts' => $posts,
            ]);
        }elseif($args["item"]==null){
            $cat = Category::select('id','name')->where('slug',$args["cat-slug"])->first();
            if(Post::exist()){
                $posts = Post::where('cat_id', $cat["id"])->get();
                if($posts->isEmpty()){
                    $status = 'OK';
                    $msg = "Posts for ".$cat["name"]." not exists!";
                }else{
                    $status = 'OK';
                    $msg = "Show posts by ".$cat["name"]." category.";
                }
            }
            $payload = json_encode(['status' => $status,'msg'=>$msg,'cat_id'=>$cat["id"],'cat_slug'=>$args["cat-slug"], 'posts' => $posts,], JSON_PRETTY_PRINT);
            $response->getBody()->write($payload);
            return $response->withHeader('Content-Type', 'application/json');
        }else{
            $posts = Post::where('parent_id', $args["id"])->get();
            $payload = json_encode(['status' => "OK",'msg'=>"Parent posts",'cat_id'=>$cat["id"],'cat_slug'=>$args["cat-slug"], 'posts' => $posts,], JSON_PRETTY_PRINT);
            $response->getBody()->write($payload);
            return $response->withHeader('Content-Type', 'application/json');
        }

    }
    public function add_cat(Request $request, Response $response){
        $body = $request->getParsedBody();
        $status = "OK";
        $cat = Category::where('slug',$body["slug"])->first();
        if($cat == null){
            Category::create([
                'name' => $body["name"],
                'slug' => $body["slug"],
                'description' => $body["description"],
            ]);
            $msg = "Category added!";
            $cat_id = Category::select('id')->where('slug',$body["slug"])->first();
        }else{
            $msg = "Category with slug ".$body["slug"]." exists!";
            $status = "fail";
        }
        $payload = json_encode(['status' => $status,'msg'=>$msg,'cat_id'=>$cat_id["id"], 'name' => $body["name"], 'slug' => $body["slug"],'request'=>$body,], JSON_PRETTY_PRINT);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json');
    }
    public function add_post(Request $request, Response $response){
        $body = $request->getParsedBody();
        $status = "OK";
        $post = Post::where('slug',$body["slug"])->first();
        if($post == null){
            Post::create([
                'title' => $body["title"],
                'slug' => $body["slug"],
                'cat_id' => $body["cat_id"],
                'parent_id' => $body["parent_id"],
                'description' => $body["description"],
                'content' => $body["content"],
            ]);
            $msg = "Post added!";
            $post = Post::select('id')->where('slug',$body["slug"])->first();
            $cat_slug = Category::select('slug')->where('id',$body["cat_id"])->first();
        }else{
            $msg = "Post with slug ".$body["slug"]." exists!";
            $status = "fail";
        }
        $payload = json_encode(['status' => $status,'msg'=>$msg, 'post_id'=>$post["id"],'parent_id'=>$body["parent_id"],'title' => $body["title"], 'slug' => $body["slug"],'cat_slug'=>$cat_slug["slug"],], JSON_PRETTY_PRINT);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json');
    }
    function upload(Request $request, Response $response,$args){
        $status = "OK";
        $msg = "uploaded!";
        $uploads = $this->get_upload_folder();
        $image = $request->getUploadedFiles();
        if($image[0]!=null)
            $clientfilename = $image[0]->getClientFilename();
        else{
            $payload = json_encode(['status' => "Oppsss...",'msg'=>'No image','image'=>$image, ], JSON_PRETTY_PRINT);
            $response->getBody()->write($payload);
            return $response->withHeader('Content-Type', 'application/json');
        }
        $filename_parts = explode(".",$clientfilename);
        $extention = array_pop ($filename_parts);
        $target = $uploads.'/'.$args["item"].'-'.$args["slug"].'-'.$args["id"].'.'.$extention;
        $image_name = $args["item"].'-'.$args["slug"].'-'.$args["id"].'.'.$extention;
        if($args["item"]=='page'){
            $target = $uploads.'/pages/'.$clientfilename;
            $image_name = $clientfilename;
        }
        $image[0]->moveTo( $target);
        
        if($args["item"]=='cat'){
            Category::find($args["id"])->update([
                'image' => $image_name,
            ]);
        }
        if($args["item"]=='post'){
            Post::find($args["id"])->update([
                'image' => $image_name,
            ]);
        }
        $payload = json_encode(['status' => $status,'msg'=>$msg,'image'=>$image_name,'path'=>$target, ], JSON_PRETTY_PRINT);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json');

    }
    function del_img(Request $request, Response $response,$args){
        $uploads = $this->get_upload_folder();
        $image_path = $uploads."/".$args["item"]."/".$args["image"];
        unlink($image_path);
        $msg = "Image deleted!";
        $payload = json_encode(['status' => 'OK','msg'=>$msg,'image'=>$image_path], JSON_PRETTY_PRINT);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json');
    }
    function del_post(Request $request, Response $response, $args){
        Post::find($args["id"])->delete();
        $msg = "Post deleted!";
        $payload = json_encode(['status' => 'OK','msg'=>$msg,], JSON_PRETTY_PRINT);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json');
    }
    function del_cat(Request $request, Response $response, $args){
        Category::find($args["id"])->delete();
        $msg = "Category deleted!";
        $payload = json_encode(['status' => 'OK','msg'=>$msg,], JSON_PRETTY_PRINT);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json');
    }
    function edit_cat(Request $request, Response $response, $args){
        $cat = Category::find($args["id"]);
        $payload = json_encode(['status' => 'OK','cat_id'=>$cat["id"],'slug'=>$cat["slug"],'name'=>$cat["name"],'cat'=>$cat,], JSON_PRETTY_PRINT);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json');
    }
    function edit_post(Request $request, Response $response, $args){
        $post = Post::find($args["id"]);
        $payload = json_encode(['status' => 'OK','post'=>$post,], JSON_PRETTY_PRINT);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json');
    }
    function update_cat(Request $request, Response $response){
        $body = $request->getParsedBody();
        Category::find($body["id"])->update([
            'name' => $body["name"],
            'description' => $body["description"],
        ]);
        $cat =Category::select('slug')->where('id',$body["id"])->first();
        $payload = json_encode(['status' => 'OK','msg'=>'Category updated!','cat_id'=>$body["id"],'name'=>$body["name"],'cat'=>$cat,], JSON_PRETTY_PRINT);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json');
    }
    function update_post(Request $request, Response $response){
        $body = $request->getParsedBody();
        Post::find($body["id"])->update([
            'title' => $body["title"],
            'cat_id' => $body["cat_id"],
            'description' => $body["description"],
            'content' => $body["content"],
        ]);
        $post =Post::select('slug')->where('id',$body["id"])->first();
        $payload = json_encode(['status' => 'OK','msg'=>'Post updated!','post_id'=>$body["id"],'title'=>$body["title"],'post'=>$post], JSON_PRETTY_PRINT);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json');
    }
    function get_upload_folder(){
        $path = $_SERVER["SCRIPT_FILENAME"];
        $pos=strripos($path, "/");
        $path=substr($path, 0, $pos);
        $pos=strripos($path, "/");
        $path=substr($path, 0, $pos);
        $path = $path."/public_html/uploads";
        return $path;
    }
}
?>