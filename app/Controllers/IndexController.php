<?php
namespace App\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use App\Models\Post;
use App\Models\Category;
use App\Models\Page;

class IndexController extends Controller
{
    public function main(Request $request, Response $response){
        $pages = Page::all();
        $categories = Category::all();
        $posts = Post::paginate(10);
        $result = "Hello!";
        return $this->view->render($response, 'index.twig', [
            'categories' => $categories,
            'posts' => $posts,
            'pages' => $pages,
        ]);
    }
    public function show_page(Request $request, Response $response,$args){
        $categories = Category::all();
        $pages = Page::all();
        $page = Page::select()->where('slug', $args["page"])->first();
        return $this->view->render($response, 'page.twig', [
            'categories' => $categories,
            'pages' => $pages,
            'page' => $page,
        ]);
    }
    public function show_post(Request $request, Response $response, $args){
        $categories = Category::all();
        $pages = Page::all();
        if($args["cat-slug"]==NULL){                                    //page blog
            $posts = Post::paginate(10);
            foreach($posts as $post){
                foreach ($categories as $cat) { 
                    if($cat["id"]==$post["cat_id"]){
                        $post["cat_id"]=$cat["slug"];
                        break;
                    }
                }
            }
            $template = 'post-list.twig';
        }elseif($args["post-slug"]==NULL){                              //page for cat
            foreach($categories as $cat){
                if($cat["slug"]==$args["cat-slug"]){
                    $cat_id = $cat["id"];
                    break;
                }
            }
            $posts = Post::select()->where([['cat_id',$cat_id],['parent_id',0]])->paginate(10);
            $template = 'post-list.twig';
        }else{                                                          //single post
            $post = Post::select('id')->where('slug',$args["post-slug"])->first();
            $posts = Post::select()->where('parent_id',$post["id"])->get();
            $template = 'single-post.twig';
        }
        return $this->view->render($response, $template, [
            'categories' => $categories,
            'posts' => $posts,
            'pages' => $pages,
            'cat_slug'=> $args["cat-slug"],
        ]);
    }
}
?>