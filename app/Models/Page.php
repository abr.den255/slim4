<?php
namespace App\Models;

use \Illuminate\Database\Schema\Blueprint;
use \Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Schema\Builder;
use App\Database;

class Page extends Model
{
	protected $table = 'pages';

	protected $fillable = ['title', 'show', 'description', 'slug', 'image', 'content',];

	public static function exist(){
        if(Database::$db->schema()->hasTable('pages'))
            return true;
        else
            return false;
    }
}