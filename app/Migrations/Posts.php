<?php
namespace App\Migrations;

use \Illuminate\Database\Schema\Builder;
use \Illuminate\Database\Schema\Blueprint;
use App\Database as Database;

class Posts
{
    static $name = 'posts';
    public static function up()
    {
        Database::$db->schema()->create(self::$name, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('cat_id')->default(1);
            $table->unsignedBigInteger('parent_id')->default(0);
            $table->string('slug')->unique();
            $table->string('title');
            $table->text('description');
            $table->string('image')->default('none.jpg');
            $table->binary('content');
            $table->timestamps();
        });
        return $message;
    }
    public static function exist(){
        if(Database::$db->schema()->hasTable(self::$name))
            return true;
        else
            return false;
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public static function down()
    {
        Database::$db->schema()->drop(self::$name);
    }
}
?>