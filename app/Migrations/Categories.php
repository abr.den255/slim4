<?php
namespace App\Migrations;

use \Illuminate\Database\Schema\Builder;
use \Illuminate\Database\Schema\Blueprint;
use App\Database as Database;

class Categories
{
    static $name = 'cats';
    public static function up()
    {
        Database::$db->schema()->create(self::$name, function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('name');
            $table->string('image')->default('none.jpg');
            $table->text('description');
            $table->timestamps();
        });
        return $message;
    }
    public static function exist(){
        if(Database::$db->schema()->hasTable(self::$name))
            return true;
        else
            return false;
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public static function down()
    {
        Database::$db->schema()->drop(self::$name);
    }
}
?>