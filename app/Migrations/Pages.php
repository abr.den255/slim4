<?php
namespace App\Migrations;

use \Illuminate\Database\Schema\Builder;
use \Illuminate\Database\Schema\Blueprint;
use App\Database as Database;

class Pages
{
    static $name = 'pages';
    public static function up()
    {
        Database::$db->schema()->create(self::$name, function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique()->default("/");;
            $table->string('title');
            $table->boolean('show')->default(true);
            $table->text('description');
            $table->string('image')->default('none.jpg');
            $table->binary('content');
            $table->timestamps();
        });
        return $message;
    }
    public static function exist(){
        if(Database::$db->schema()->hasTable(self::$name))
            return true;
        else
            return false;
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public static function down()
    {
        Database::$db->schema()->drop(self::$name);
    }
}
?>